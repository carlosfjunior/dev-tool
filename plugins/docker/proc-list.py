import subprocess, json
from terminaltables import AsciiTable
from termcolor import colored

try:
    containers = json.loads(subprocess.check_output("docker inspect $(docker ps -aq)", shell=True, stderr=subprocess.PIPE).decode('ascii'))
except Exception as e:
    print("Nenhum container para listar.")
    exit()

table_data = [
    ['CONTAINER/POOL', 'DOMAINS', 'STATUS', 'IMAGE', ]
]

containers = json.loads(subprocess.check_output("docker inspect $(docker ps -aq)", shell=True).decode('ascii'))

for container in containers:
    name = container['Name']
    status = container['State']['Status']
    urls = container['Config']['Env']
    urls = [x.split("=")[1] for x in urls if "VIRTUAL_HOST" in x]
    urls = '\n'.join(set(['\n'.join(x.split(',')) for x in urls if x != ""]))
    img = container['Config']['Image'].split('/')
    img = img[1] if len(img) > 1 else img[0]

    if status == 'running':
        status = colored(status, 'green')
    else:
        status = colored(status, 'red')

    table_data.append([name, urls, status, img])

table = AsciiTable(table_data)
table.inner_row_border = True
table.justify_columns[1] = 'right'
print(table.table)
