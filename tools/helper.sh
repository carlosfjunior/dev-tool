#!/bin/bash


message_sucess() {
        local message=$1
        printf '\n'
        printf "\e[42;5;124m"
        printf "${bold} $message \n"
        printf '\n'
        printf '\e[0m'
}

message_error2() {
        local message=$1
        printf '\n'
        printf "\e[48;5;124m"
        printf '\n'
        printf "${bold} $message \n"
        printf '\n'
        printf '\e[0m'
	${reset}
}


message_error() {
        local message=$1
        printf '\n'
        printf "\e[48;5;124m"
        printf '\n'
        printf "${bold} $message \n"
        printf '\n'
        printf '\e[0m'
        exit 1
}

bold=$(tput bold)
italic=$(tput sitm)
underline=$(tput smul)
nounderline=$(tput rmul)
reset=$(tput sgr0)
red=$(tput setaf 1)
blue=$(tput setaf 4)
yellow=$(tput setaf 3)
green=$(tput setaf 2)
